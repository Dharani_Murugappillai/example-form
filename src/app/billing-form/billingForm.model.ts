export class billingDetails{
    code:number;
    productName:string;
    quantity:number;
    price:number;
    total:number;
}