import { Component, OnInit } from '@angular/core';

import { billingDetails } from './billingForm.model';

@Component({
  selector: 'app-billing-form',
  templateUrl: './billing-form.component.html',
  styleUrls: ['./billing-form.component.css']
})
export class BillingFormComponent implements OnInit {

  billingDetails=new billingDetails();
  dataArray=[];
  

  constructor() { }

  ngOnInit(): void {
    this.dataArray.push(this.billingDetails);
    
  }

  addForm(){

   
    this.billingDetails=new billingDetails;
    this.dataArray.push(this.billingDetails);

  }

  removeForm(){
    this.dataArray.pop();
  }

}
