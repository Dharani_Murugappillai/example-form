import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';


// Materials 

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DemoMaterialModule } from './material-module';
import { BillingFormComponent } from './billing-form/billing-form.component';

const appRoutes : Routes = [
  {path:'login',component:LoginComponent,},
  {path:'', redirectTo:'login', pathMatch:'full' }
]; 


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BillingFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, {useHash:true}),
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    NgbModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
